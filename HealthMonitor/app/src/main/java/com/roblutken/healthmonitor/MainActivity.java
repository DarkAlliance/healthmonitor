package com.roblutken.healthmonitor;

import android.app.ActionBar;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private DrawerLayout drawerLayout;
    private ListView listView;
    private NavigationAdapter navigationAdapter;
    private ActionBarDrawerToggle drawerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout=(DrawerLayout)findViewById(R.id.drawerLayout);

        listView=(ListView)findViewById(R.id.drawerList);
        navigationAdapter = new NavigationAdapter(this);
        listView.setAdapter(navigationAdapter);

        drawerListener = new ActionBarDrawerToggle(this,drawerLayout,R.string.open_menu,R.string.close_menu){
            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }
        };
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_dashboard);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.green)));
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayShowTitleEnabled(true);

        drawerLayout.setDrawerListener(drawerListener);


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerListener.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(drawerListener.onOptionsItemSelected(item))
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        drawerListener.syncState();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        selectItem(position);
    }

    private void selectItem(int position) {
        listView.setItemChecked(position,true);

    }
    private void setTitle(String title)
    {
        getSupportActionBar().setTitle(title);
    }
}
class NavigationAdapter extends BaseAdapter
{
    private Context context;
    String[] navItems;
    int[] icons = {R.drawable.ic_dashboard,R.drawable.ic_bmi,R.drawable.ic_exercise,R.drawable.ic_food,R.drawable.ic_weight};
    public NavigationAdapter(Context context)
    {
        this.context = context;
        navItems = context.getResources().getStringArray(R.array.navItems);

    }
    @Override
    public int getCount() {
        return navItems.length;
    }

    @Override
    public Object getItem(int position) {
        return navItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = null;
        // If we're creating for the first time
        if(convertView == null )
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.navigation_row,parent,false);
        }
        else {
            row = convertView;
        }
       TextView menuTitleText = (TextView) row.findViewById(R.id.MenuTitle);
        ImageView menuIcon = (ImageView) row.findViewById(R.id.MenuIcon);

        menuTitleText.setText(navItems[position]);
        menuIcon.setImageResource(icons[position]);
        return row;
    }
}