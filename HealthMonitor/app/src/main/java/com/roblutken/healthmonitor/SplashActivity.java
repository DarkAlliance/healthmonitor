package com.roblutken.healthmonitor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Rob on 30/03/2015.
 */
public class SplashActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Set Fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        /*
            Create a thread to sleep for 5 seconds
            then invoke the next screen
         */

        Thread splashTimer = new Thread()
        {
            public void run()
            {
                try
                {
                    // Sleep for x Amount
                    sleep(0001);
                    // Create the next event
                    Intent intent = new Intent(getBaseContext(),MainActivity.class);
                    startActivity(intent);
                    // Remove activity
                    finish();
                }catch (InterruptedException ex)
                {
                    ex.printStackTrace();
                }
            }

        };
        splashTimer.start();
    }

}
