package com.rol.HealthMonitor;




import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class About extends Fragment {

	
	 
	@Override 
	public View onCreateView(LayoutInflater inflate,ViewGroup container,Bundle savedInstanceState)
	{
		return inflate.inflate(R.layout.fragment_about,container,false);
		
	}
}
