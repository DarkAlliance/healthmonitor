package com.rol.HealthMonitor;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.DecimalFormat;

public class BmiActivity extends Fragment {

    Time today = new Time(Time.getCurrentTimezone());
    private EditText et_age, et_height, et_weight;
    private ToggleButton tb_unit;
    private Button bt_calculate;
    private BMIInfo bmiInfo;
    private String BMICat, BMIScore, heightUnits, weightUnits;
    private DBAdapter mDb;
    private ListView bmiList;
    // Gets a writable database
    private void openDB()
    {
        mDb = new DBAdapter(getActivity());
        mDb.open();
    }
	@Override
	public View onCreateView(LayoutInflater inflate,ViewGroup container,Bundle savedInstanceState)
	{
        // Inflate the fragment view
        final View bmiView = inflate.inflate(R.layout.fragment_bmi,container,false);
        // Initalise default settings
        initialiseActivity(bmiView);

        tb_unit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bmiInfo.useMetric = true;
                    weightUnits = "Kg";
                    heightUnits = "M";
                    et_weight.setHint("Enter Weight (Kilograms)");
                    et_height.setHint("Enter Height (Meters)");
                } else {
                    bmiInfo.useMetric = false;
                    weightUnits = "lb";
                    heightUnits = "ft";
                    et_weight.setHint("Enter Weight (Pounds)");
                    et_height.setHint("Enter Height (Feet)");
                }
            }
        });
        // Show user info
        bt_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check everything has been filled in
                if (!findEmpty(v)) {
                    // Create a new user
                    // Parse weight and height to strings to float values
                    parseBMIEntry();

                    // Display results to the user
                    new AlertDialog.Builder(bmiView.getContext())
                            .setTitle("Your BMI")
                            .setMessage("Your BMI Score:\n" + BMIScore + "\nYour BMI Classification:\n" + BMICat)
                            .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Adds information into the database
                                    saveBMIData(bmiInfo);
                                    // Re-populate the list view
                                    populateListView(bmiView);
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                }
            }
        });
		return bmiView;
	}

    // Parse the edit text's and performs BMI calculations
    private void parseBMIEntry() {
        try {
            float weight = Float.parseFloat(et_weight.getText().toString());
            float height = Float.parseFloat(et_height.getText().toString());
            // Calculate the users bmi
            BMICat = bmiInfo.GetClassification(weight, height);
            BMIScore = new DecimalFormat("#.##").format(bmiInfo.BMIScore);
        } catch (Exception ex) {
            Log.v("BMI Error", ex.getMessage());
        }
    }
    // Inserts new entry into the database
    private void saveBMIData(BMIInfo info)
    {
        today.setToNow();
        String timestamp = today.format("%d-%m-%Y\n%H:%M:%S");

        mDb.insertBMIRow(timestamp,
                String.format("Age: %s", String.valueOf(et_age.getText())),
                String.format("Weight: %s%S", String.valueOf(et_weight.getText()), weightUnits),
                String.format("Height: %s%s", String.valueOf(et_height.getText()), heightUnits),
                "BMI Score: " + BMIScore,
                "Status: " + BMICat);

    }
    private void populateListView(View v)
    {
        // Get all of the info from database
        Cursor cursor = mDb.getAllBmi();

        // Match up the database rows to the custom list view (bmi_item_layout)
        String[] rowNames = new String[]{DBAdapter.KEY_DATE,DBAdapter.KEY_WEIGHT,DBAdapter.KEY_BMISCORE,DBAdapter.KEY_BMICLASSIFICATION};
        int[] toViewIDs = new int[]{R.id.tv_item_date,R.id.tv_item_bmi_weight,R.id.tv_item_bmi_score,R.id.tv_item_bmi_classification};
        // Setup the cursor adapter with the custom layout
        SimpleCursorAdapter mCursorAdapter;
        mCursorAdapter = new SimpleCursorAdapter(getActivity().getBaseContext(),R.layout.bmi_item_layout,cursor,rowNames,toViewIDs,0);

        //Set adapter to list view
        bmiList.setAdapter(mCursorAdapter);

    }

    private void updateBMIEntry(long id)
    {
        Cursor c = mDb.getBmiRow(id);
        Toast.makeText(getActivity(), "Editing Record", Toast.LENGTH_LONG).show();
        parseBMIEntry();
        if(c.moveToFirst()){
            today.setToNow();
            String date = today.format("%d-%m-%Y\n%H:%M:%S");
            String newAge = et_age.getText().toString();
            String newHeight = et_height.getText().toString();
            String newWeight = et_weight.getText().toString();
            String newBMICat = BMICat;
            String newBMIScore = BMIScore;
            mDb.updateBmiRow(id, date, newAge, newWeight, newHeight, newBMIScore, newBMICat);
        }
        c.close();
    }

    private void listViewItemLongClick(View v) {


        bmiList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mDb.deleteBMIRow(id);
                Toast.makeText(view.getContext(), "BMI Info deleted!", Toast.LENGTH_SHORT).show();
                populateListView(view);
                return false;
            }
        });
    }
    private void listViewItemClick(View v) {
        ListView lv = (ListView) v.findViewById(R.id.listViewBMI);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateBMIEntry(id);
                populateListView(view);
            }
        });
    }

    private boolean findEmpty(View v)
    {
        // Check each edit text for empty strings
        if(et_age.getText().toString().matches(""))
        {
            Toast.makeText(v.getContext(),"Please enter an age.", Toast.LENGTH_LONG).show();
            return true;
        }
        else if(et_weight.getText().toString().matches(""))
        {
            Toast.makeText(v.getContext(),"Please enter an weight.", Toast.LENGTH_LONG).show();
            return true;
        }
        if(et_height.getText().toString().matches(""))
        {
            Toast.makeText(v.getContext(),"Please enter an height.", Toast.LENGTH_LONG).show();
            return true;
        }
        // Otherwise return that no empty fields were found
        return false;
    }

    // Initialises the BMI Activity inital values
    private void initialiseActivity(View bmiView) {
        bmiInfo = new BMIInfo();
        // Open our  database
        openDB();
        bmiList = (ListView) bmiView.findViewById(R.id.listViewBMI);
        // Populate the list view
        populateListView(bmiView);
        // Setup UI Widgets
        et_age = (EditText) bmiView.findViewById(R.id.et_age);
        et_weight = (EditText) bmiView.findViewById(R.id.et_weight);
        et_height = (EditText) bmiView.findViewById(R.id.et_height);
        bt_calculate = (Button) bmiView.findViewById(R.id.btn_calculate);
        tb_unit = (ToggleButton) bmiView.findViewById(R.id.unitToggle);
        // Activate on list item listener
        listViewItemClick(bmiView);
        // Activate on long item listener
        listViewItemLongClick(bmiView);
        // Set Text box defaults
        et_weight.setHint("Enter Weight (Pounds)");
        et_height.setHint("Enter Height (Feet)");
        weightUnits = "lb";
        heightUnits = "ft";
    }
    /* NOT USED converted to database
    public void WritePreferences()
    {
        Context context = getActivity();
        SharedPreferences sharedPrefs = context.getSharedPreferences(getString(R.string.preferences_key_file),Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt(getString(R.string.usr_age),usr.age);
        editor.putFloat(getString(R.string.usr_weight),usr.weight);
        editor.putFloat(getString(R.string.usr_height),usr.height);
        editor.putFloat(getString(R.string.usr_bmi_score),usr.bmi);
        editor.putString(getString(R.string.usr_bmi_classification),usr.BMIClassification());
        editor.commit();
    }
    public void ReadPreferences()
    {
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        float highScore = sharedPref.getFloat(getString(R.string.usr_height),0);
    }
*/


}
