package com.rol.HealthMonitor;

/**
 * Created by Rob on 06/05/2015.
 */
public class TargetInfo {
    public String TargetWeight;
    public String TargetTimeFrame;
    public String TargetLossRatePerDay;

    public TargetInfo(String weight, String timeframe, String lossRate)
    {
        this.TargetWeight = weight;
        this.TargetTimeFrame = timeframe;
        this.TargetLossRatePerDay = lossRate;
    }
    public boolean SaveTargetDB(DBAdapter db)
    {
        if(db.getAllTargets().getCount() <= 0) {
            db.insertTargetRow(this.TargetWeight, this.TargetTimeFrame, this.TargetLossRatePerDay);
        }
        else
            db.updateTargetRow(1,this.TargetWeight,this.TargetTimeFrame,this.TargetLossRatePerDay);

        return true;
    }

}
