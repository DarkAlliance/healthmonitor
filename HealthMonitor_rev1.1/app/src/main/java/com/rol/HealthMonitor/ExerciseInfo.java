package com.rol.HealthMonitor;

/**
 * Created by Rob on 07/04/2015.
 */
public class ExerciseInfo {
    private int id;
    private String date;
    private String startTime;
    private String activityType;
    private float duration;
    private float caloriesBurnt;
    public ExerciseInfo(){}
    public ExerciseInfo(String _date,String _startTime,String _activity,float _duration,float _caloriesBurnt)
    {
        super();
        date = _date;
        startTime = _startTime;
        activityType = _activity;
        duration = _duration;
        caloriesBurnt = _caloriesBurnt;
    }
    public String toString()
    {
        return "Exercise Activity [id=" + id +
                ", Date:\n"+date+
                "\nStart:\n"+startTime+
                "\nActivity Type:\n"+activityType+
                "\nDuration:\n"+duration+
                "\nCalories Burnt:\n"+caloriesBurnt+
                "]";
    }
    public int GetID(){return id;}
    public String GetDate(){return date;}
    public String GetTime(){return startTime;}
    public String GetActivityType(){return activityType;}
    public float GetDuration(){return duration;}
    public float GetCaloriesBurnt(){return caloriesBurnt;}
    public void SetID(int _id){id = _id;}
    public void SetDate(String _date){date = _date;}
    public void SetTime(String _time){startTime = _time;}
    public void SetActivityType(String _activity){activityType = _activity;}
    public void SetDuration(float _dur  ){duration = _dur;}
    public void SetCaloriesBurnt(float _calories){caloriesBurnt = _calories;}
}
