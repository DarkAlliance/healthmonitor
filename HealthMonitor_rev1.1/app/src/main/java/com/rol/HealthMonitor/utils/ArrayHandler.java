package com.rol.HealthMonitor.utils;

import java.util.ArrayList;
import java.util.List;

public class ArrayHandler {
	
	private List<Object> arr = new ArrayList<Object>();
	public void ArrayHandler()
	{
		
	}
	
	public void addEntry(String label,float val)
	{
		ArrayObj arr1 = new ArrayObj();
		arr1.setTitle(label);
		arr1.setValue(val);
		arr.add(arr1);
	}
	public List<Object> getArray()
	{
		return arr;
	}
	private class ArrayObj{
		
		public double subValue;
		public String titleValue;
		
		public double getValue(){return this.subValue;}
		public String getTitle(){return this.titleValue;}
		
		public void setValue(double arg){this.subValue = arg;}
		public void setTitle(String arg){this.titleValue = arg;}
		
	}
}

