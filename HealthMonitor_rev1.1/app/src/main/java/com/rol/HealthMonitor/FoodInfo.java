package com.rol.HealthMonitor;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rob on 01/05/2015.
 */
public class FoodInfo {

    // Currently selected food item
    public String selectedFood;
    // Portion size of a given item
    public float portionSize;
    // Number of calories consumed
    public float caloriesConsumed;
    // List of items for spinner
    ArrayList<String> items;
    // Hashmap with FOOD ITEM, NUMBER OF CALORIES
    HashMap<String, Integer> foods = new HashMap<String, Integer>();

    // Default constructor - adds food items to hashmap
    public FoodInfo() {

        foods.clear();
        // All of my favorite foods!
        // If time allowed I would have pulled these from www.nutritionix.com/api
        // Information sourced from http://www.weightlossresources.co.uk/calories/calorie_counter.htm
        foods.put("White Toast", 87);
        foods.put("Cornflakes 45g", 144);
        foods.put("Crunchy Nut 45g", 176);
        foods.put("Kebab 168g", 429);
        foods.put("Snickers 1 bar", 323);
        foods.put("Cheese and Onion crisps 1 bag", 184);
        foods.put("Tea 1 mug", 29);
        foods.put("Whole Milk 30ml", 20);
        foods.put("Coffee 1 mug", 15);
        foods.put("Coke 330ml", 139);

    }

    // Get the calories associated with a given food
    public int GetValues(String lookup) {
        return foods.get(lookup);
    }

    // Return a complete list of keys in the foods hashmap
    public ArrayList<String> GetKeys() {
        items = new ArrayList<String>(foods.keySet());
        return items;
    }

    // Return the portion size
    public float GetPortionSize() {
        return portionSize;
    }
}
