package com.rol.HealthMonitor;

import android.util.Log;

/**
 * Created by Rob on 06/04/2015.
 */
public class BMIInfo{
    public float BMIScore;
    public boolean useMetric;
    public float weight;
    public float height;

    public BMIInfo()
    {
        Log.v("BMI calc", "BMI Calc created");
    }

    //
    //  Returns the classification of the user's BMI
    //
    public String GetClassification(float _weight, float _height) {

        BMIScore = CalculateBMI(_weight, _height);
        weight = _weight;
        height = _height;
        //Log.d("BMI Calc",String.valueOf(BMIScore));
        //Log.d("BMI Calc",String.valueOf(_weight) + "\n" + String.valueOf(_height));
        if (BMIScore < 15.0) return "Very severely underweight";
        else if (BMIScore >= 15.0 && BMIScore <= 16.0) return "Severely underweight";
        else if (BMIScore >= 16.0 && BMIScore <= 18.5) return "Underweight";
        else if (BMIScore >= 18.5 && BMIScore <= 25.0) return "Healthy weight";
        else if (BMIScore >= 25.0 && BMIScore <= 30.0) return "Overweight";
        else if (BMIScore >= 30.0 && BMIScore <= 35.0) return "Moderately obese";
        else if (BMIScore >= 35.0 && BMIScore <= 40.0) return "Severely obese";
        else return "Very severely obese";

    }

    //
    //  Calculates the users BMI based upon unit Metric or Imperial system
    //
    public float CalculateBMI(float _weight, float _height) {
        if (useMetric) {
            Log.v("BMI calc", "BMI Metric SCORE:" + (_weight / (_height * _height)));
            return (_weight / (_height * _height));
        } else {
            Log.v("BMI calc", "BMI Imperial SCORE:" + (_weight / (_height * _height)) * 703);
            return (_weight / (_height * _height)) * 703;
        }

    }

    public float GetWeight()
    {
        return this.weight;
    }

    public float GetHeight() {
        return this.height;
    }
}