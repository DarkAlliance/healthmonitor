package com.rol.HealthMonitor;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;


public class DashboardActivity extends Fragment {

    Time today = new Time(Time.getCurrentTimezone());
    private Button bt_set_target, bt_calc_bmi, bt_food_entry, bt_exercise_tracker;
    private TextView tv_current_weight, tv_today_exercise, tv_today_food, tv_current_target,
            tv_calorie_advice;
    private DBAdapter mDb;
    private float targetWeight = 0;
    private int targetWeeks = 0;

    @Override
    public View onCreateView(LayoutInflater inflate, ViewGroup container, Bundle savedInstanceState) {
        final View dashView = inflate.inflate(R.layout.fragment_dashboard, container, false);
        initialiseDashboard(dashView);
        return dashView;
    }

    // Gets a writable database
    private void openDB() {
        mDb = new DBAdapter(getActivity());
        mDb.open();
    }

    // Initalises the UI widgets and loads the activity
    private void initialiseDashboard(View dashView) {
        // Open our  database
        openDB();

        tv_current_weight = (TextView) dashView.findViewById(R.id.tv_current_weight);
        tv_today_exercise = (TextView) dashView.findViewById(R.id.tv_calories_burnt);
        tv_today_food = (TextView) dashView.findViewById(R.id.tv_calories_consumed);
        tv_current_target = (TextView) dashView.findViewById(R.id.tv_current_target);
        tv_calorie_advice = (TextView) dashView.findViewById(R.id.tv_calorie_advice);

        bt_set_target = (Button) dashView.findViewById(R.id.bt_set_target);
        bt_calc_bmi = (Button) dashView.findViewById(R.id.bt_bmi);
        bt_food_entry = (Button) dashView.findViewById(R.id.bt_food_entry);
        bt_exercise_tracker = (Button) dashView.findViewById(R.id.bt_exercise);

        initialiseButtonHandlers();
        calcTodayCalorieBurn();
        calcTodayCalorieConsumed();
        getCurrentWeight();
        getTargetWeight();
    }

    // Loads and initalises the button handlers
    private void initialiseButtonHandlers() {
        bt_set_target.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateDialog().show();
            }
        });
        bt_calc_bmi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new BmiActivity();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, mFragment).commit();
            }
        });
        bt_food_entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new FoodActivity();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, mFragment).commit();
            }
        });
        bt_exercise_tracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new ExerciseActivity();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, mFragment).commit();
            }
        });
    }

    // Calculates how many calories have been burnt today
    private void calcTodayCalorieBurn() {
        today.setToNow();
        String timestamp = today.format("%d-%m-%Y");
        tv_today_exercise.setText(tv_today_exercise.getText().toString() + ": " + String.valueOf(new DecimalFormat("#.##").format(mDb
                .getActivityToday
                        (timestamp))));
    }

    // Calculates calories eaten today
    private void calcTodayCalorieConsumed() {
        today.setToNow();
        String timestamp = today.format("%d-%m-%Y");
        tv_today_food.setText(tv_today_food.getText().toString() + ": " + String.valueOf(new DecimalFormat("#.##").format(mDb
                .getFoodToday
                        (timestamp))) + " / 1,900");
    }

    // Gets the latest weight entry in BMI Table
    private void getCurrentWeight() {
        String weight = "";
        // Get the latest bmi entry
        Cursor c = mDb.getLastBmiRow();
        if (c.getCount() > 0) {
            // Weight is the third column
            weight = c.getString(3);
            // Re-format back into a float and return
            tv_current_weight.setText(tv_current_weight.getText().toString() + ": " +
                    Float.valueOf(weight.replaceAll("[^0-9?!\\.]", "")));
        } else if (c.getCount() <= 0) {
            Toast.makeText(getActivity().getBaseContext(), "Please enter your BMI information for more accurate results.", Toast.LENGTH_LONG).show();
            tv_current_weight.setText(tv_current_weight.getText().toString() + " Not set");
        }
    }

    // Gets target weight from database (if any)
    private void getTargetWeight() {
        String targetWeight = "";
        String targetTimeframe = "";
        String targetAdvice = "";
        String currentWeightString = "";
        Cursor c = mDb.getAllTargets();
        Cursor bmiCursor = mDb.getLastBmiRow();
        if (c.getCount() > 0 && bmiCursor.getCount() > 0) {
            targetWeight = c.getString(1);
            targetTimeframe = c.getString(2);
            targetAdvice = c.getString(3);
            currentWeightString = bmiCursor.getString(3);
            tv_current_target.setText("Current target: " + targetWeight + " in " + targetTimeframe + " weeks");
            float currentWeight = Float.valueOf(currentWeightString.replaceAll("[^0-9?!\\.]", ""));
            Log.v("Dash activity", currentWeight + "\n" + targetWeight);
            if (currentWeight > Float.valueOf(targetWeight)) {
                tv_calorie_advice.setText("You need to loose " + String.format("%.2s", targetAdvice) + " calories " +
                        "today");
            } else {
                tv_calorie_advice.setText("You need to gain " + String.format("%.2s", targetAdvice) + " calories " +
                        "today");
            }
        }
    }

    // Update the target advice
    private void updateTarget() {
        calcTargetLoss();
        tv_current_target.setText("Current target: " + String.valueOf
                (targetWeight) + " in " + String.valueOf(targetWeeks) + " weeks");
    }

    // Calculates how many caloires needed to burn / gain today
    private void calcTargetLoss() {
        String weight = "";
        String height = "";
        String age = "";
        float calorieGainLoss = 0;
        Cursor c = mDb.getLastBmiRow();
        if (c.getCount() > 0) {
            // Age is 1 column
            age = c.getString(2);
            // Weight is the third column
            weight = c.getString(3);
            // Weight is the third column
            height = c.getString(4);
            // Re-format back into a float and return
            float currentWeight = Float.valueOf(weight.replaceAll("[^0-9?!\\.]", ""));
            float currentHeight = Float.valueOf(height.replaceAll("[^0-9?!\\.]", ""));
            float currentAge = Float.valueOf(age.replaceAll("[^0-9?!\\.]", ""));

            // This Calorie Calculator is based on the Mifflin - St Jeor equation. With this equation, the Basal Metabolic Rate (BMR) is calculated by using the following formula:
            // BMR = 10 * weight(kg) + 6.25 * height(cm) - 5 * age(y) + 5 (man)
            // Convert height from meters to cm
            currentHeight = currentHeight * 100.0f;
            float BMR = (10 * currentWeight) + (6.25f * currentHeight) - (5 * currentAge) + 5;
            Log.v("BMR", String.valueOf(BMR));
            float totalCaloriesRequired = BMR * 1.2f;
            // 1 calorie to kg = 4.187 http://www.calculator.net/calorie-calculator
            // .html?ctype=metric&cage=21&csex=m&cheightfeet=5&cheightinch=10&cpound=160&cheightmeter=170&ckg=58&cactivity=1.2&printit=0&x=66&y=13
            if (currentWeight > targetWeight) {
                // you need to loose
                calorieGainLoss = (totalCaloriesRequired - 4.1868f) / targetWeeks;
                tv_calorie_advice.setText("You need to loose " + new DecimalFormat("#.##").format(calorieGainLoss) + " calories " +
                        "today");

            } else {
                calorieGainLoss = (totalCaloriesRequired + 4.1868f) / targetWeeks;
                tv_calorie_advice.setText("You need to gain " + new DecimalFormat("#.##").format(calorieGainLoss) + " calories " +
                        "today");
            }
            // Save this information into the database
            TargetInfo targetInfo = new TargetInfo(String.valueOf(targetWeight),
                    String.valueOf(targetWeeks),
                    String.valueOf(calorieGainLoss));
            targetInfo.SaveTargetDB(mDb);
        }
    }

    // Creates a Set Target custom dialog
    public Dialog onCreateDialog() {

        // Get the layout inflater
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View diagView = inflater.inflate(R.layout.dialog_layout, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final EditText et_target_set = (EditText) diagView.findViewById(R.id.et_dialog_target);

        final EditText et_timeframe = (EditText) diagView.findViewById(R.id.et_set_time_frame);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(diagView)
                .setTitle("Set your target weight (KG)")
                .setIcon(android.R.drawable.ic_dialog_info)
                        // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (et_target_set.getText().toString().isEmpty() || et_timeframe.getText()
                                .toString().isEmpty()) {
                            dialog.dismiss();
                        } else {
                            targetWeight = Float.parseFloat(et_target_set.getText().toString());
                            targetWeeks = Integer.parseInt(et_timeframe.getText().toString());
                            updateTarget();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });


        return builder.create();
    }


}


