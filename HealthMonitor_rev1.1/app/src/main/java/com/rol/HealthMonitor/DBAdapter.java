package com.rol.HealthMonitor;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Rob on 05/05/2015.
 */
public class DBAdapter {

    //BMI Row Names
    public static final String KEY_ROWID = "_id";
    public static final String KEY_DATE = "date";
    public static final String KEY_AGE = "age";
    public static final String KEY_WEIGHT = "weight";
    public static final String KEY_HEIGHT = "height";
    public static final String KEY_BMISCORE = "bmi_score";
    public static final String KEY_BMICLASSIFICATION = "bmi_classification";
    public static final String[] ALL_BMI_KEYS = new String[]{KEY_ROWID, KEY_DATE, KEY_AGE, KEY_WEIGHT, KEY_HEIGHT, KEY_BMISCORE, KEY_BMICLASSIFICATION};
    // Exercise Table Columns
    public static final String KEY_ACTIVITY_ID = "_id";
    public static final String KEY_ACTIVITY_DATE = "ACTIVITY_DATE";
    public static final String KEY_ACTIVITY_DURATION = "ACTIVITY_DURATION";
    public static final String KEY_ACTIVITY_EXERCISE = "ACTIVITY_EXERCISE";
    public static final String KEY_ACTIVITY_CALORIES_BURNT = "ACTIVITY_CALORIES_BURNT";
    public static final String[] ALL_ACTIVITY_KEYS = {KEY_ACTIVITY_ID, KEY_ACTIVITY_DATE, KEY_ACTIVITY_DURATION, KEY_ACTIVITY_EXERCISE, KEY_ACTIVITY_CALORIES_BURNT};
    // Diet Table Columns
    public static final String KEY_FOOD_ID = "_id";
    public static final String KEY_FOOD_DATE = "FOOD_DATE";
    public static final String KEY_FOOD_ITEM = "FOOD_ITEM";
    public static final String KEY_FOOD_PORTION = "FOOD_PORTION";
    public static final String KEY_FOOD_CALORIES = "FOOD_CALORIES";
    public static final String[] ALL_FOOD_KEYS = {KEY_FOOD_ID, KEY_FOOD_DATE, KEY_FOOD_ITEM, KEY_FOOD_PORTION, KEY_FOOD_CALORIES};
    // Target Table Columns
    public static final String KEY_TARGET_ID = "_id";
    public static final String KEY_TARGET_WEIGHT = "TARGET_WEIGHT";
    public static final String KEY_TARGET_TIME = "TARGET_TIME";
    public static final String KEY_TARGET_LOSS_RATE = "TARGET_LOSS_RATE";
    public static final String[] ALL_TARGET_KEYS = {KEY_TARGET_ID, KEY_TARGET_WEIGHT,
            KEY_TARGET_TIME, KEY_TARGET_LOSS_RATE};
    //Database Constructs
    public static final String DATABASE_NAME = "healthdb";
    public static final String TABLE_BMI = "bmiTable";
    public static final String TABLE_ACTIVITY = "exerciseTable";
    public static final String TABLE_FOOD = "foodTable";
    public static final String TABLE_TARGET = "targetTable";
    public static final int DATABASE_VERSION = 5;

    private static final String TAG = "Database Adapter";// used for logging
    // SQL Statement to create Database
    private static final String CREATE_TABLE_BMI = "CREATE TABLE " + TABLE_BMI +
            " (" +
            KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_DATE + " TEXT NOT NULL, " +
            KEY_AGE + " TEXT NOT NULL, " +
            KEY_WEIGHT + " TEXT NOT NULL, " +
            KEY_HEIGHT + " TEXT NOT NULL, " +
            KEY_BMISCORE + " TEXT NOT NULL, " +
            KEY_BMICLASSIFICATION + " TEXT NOT NULL" +
            ");";
    private static final String CREATE_TABLE_ACTIVITY = "CREATE TABLE " + TABLE_ACTIVITY +
            "( " +
            KEY_ACTIVITY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            KEY_ACTIVITY_DATE + " TEXT NOT NULL, " +
            KEY_ACTIVITY_DURATION + " TEXT NOT NULL, " +
            KEY_ACTIVITY_EXERCISE + " TEXT NOT NULL, " +
            KEY_ACTIVITY_CALORIES_BURNT + " TEXT NOT NULL " +
            ")";
    private static final String CREATE_TABLE_FOOD = "CREATE TABLE " + TABLE_FOOD +
            "( " +
            KEY_FOOD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            KEY_FOOD_DATE + " TEXT NOT NULL, " +
            KEY_FOOD_ITEM + " TEXT NOT NULL, " +
            KEY_FOOD_PORTION + " TEXT NOT NULL, " +
            KEY_FOOD_CALORIES + " TEXT NOT NULL " +
            ")";
    private static final String CREATE_TABLE_TARGET = "CREATE TABLE " + TABLE_TARGET +
            "( " +
            KEY_TARGET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            KEY_TARGET_WEIGHT + " TEXT NOT NULL, " +
            KEY_TARGET_TIME + " TEXT NOT NULL, " +
            KEY_TARGET_LOSS_RATE + " TEXT NOT NULL" +
            ")";
    private final Context context;
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase db;

    // Initalize the context and create a new Database helper passing in the context
    public DBAdapter(Context context) {
        this.context = context;
        mDBHelper = new DatabaseHelper(context);
    }

    // Open the database connection
    public DBAdapter open() {
        db = mDBHelper.getWritableDatabase();
        return this;
    }

    //Close the database connetion
    public void close() {
        mDBHelper.close();
    }

    //Add new BMI values into database
    public long insertBMIRow(String date, String age, String weight, String height, String bmiScore, String bmiClassification) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_DATE, date);
        cv.put(KEY_AGE, age);
        cv.put(KEY_WEIGHT, weight);
        cv.put(KEY_HEIGHT, height);
        cv.put(KEY_BMISCORE, bmiScore);
        cv.put(KEY_BMICLASSIFICATION, bmiClassification);
        // Insert data into database
        return db.insert(TABLE_BMI, null, cv);
    }

    // Add new Activity into the database
    public long insertActivityRow(String date, String duration, String exercise, String caloriesBurnt) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_ACTIVITY_DATE, date);
        cv.put(KEY_ACTIVITY_DURATION, duration);
        cv.put(KEY_ACTIVITY_EXERCISE, exercise);
        cv.put(KEY_ACTIVITY_CALORIES_BURNT, caloriesBurnt);
        // Insert data into database
        return db.insert(TABLE_ACTIVITY, null, cv);
    }

    //Add new Food values into database
    public long insertFoodRow(String date, String item, String portion, String calories) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_FOOD_DATE, date);
        cv.put(KEY_FOOD_ITEM, item);
        cv.put(KEY_FOOD_PORTION, portion);
        cv.put(KEY_FOOD_CALORIES, calories);
        // Insert data into database
        return db.insert(TABLE_FOOD, null, cv);
    }

    // Add new Target values into database
    public long insertTargetRow(String target, String timeframe, String caloriesRequired) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_TARGET_WEIGHT, target);
        cv.put(KEY_TARGET_TIME, timeframe);
        cv.put(KEY_TARGET_LOSS_RATE, caloriesRequired);
        return db.insert(TABLE_TARGET, null, cv);
    }

    // Delete row from bmi table
    public boolean deleteBMIRow(long rowID) {
        String where = KEY_ROWID + "=" + rowID;
        return db.delete(TABLE_BMI, where, null) != 0;
    }

    // Delete row from activity table
    public boolean deleteActivityRow(long rowID) {
        String where = KEY_ACTIVITY_ID + "=" + rowID;
        return db.delete(TABLE_ACTIVITY, where, null) != 0;
    }

    // Delete row from food table
    public boolean deleteFoodRow(long rowID) {
        String where = KEY_FOOD_ID + "=" + rowID;
        return db.delete(TABLE_FOOD, where, null) != 0;
    }

    // Delete row from food table
    public boolean deleteTargetRow(long rowID) {
        String where = KEY_TARGET_ID + "=" + rowID;
        return db.delete(TABLE_TARGET, where, null) != 0;
    }

    // Deletes all rows from the bmi table
    public void deleteAllBMI() {
        Cursor c = getAllBmi();
        long rowID = c.getColumnIndexOrThrow(KEY_ROWID);
        if (c.moveToFirst()) {
            do {
                deleteBMIRow(c.getLong((int) rowID));

            } while (c.moveToNext());
        }
        c.close();
    }

    // Deletes all exercises from the activity table
    public void deleteAllActivities() {
        Cursor c = getAllActivities();
        long rowID = c.getColumnIndexOrThrow(KEY_ACTIVITY_ID);
        if (c.moveToFirst()) {
            do {
                deleteActivityRow(c.getLong((int) rowID));
            } while (c.moveToNext());
        }
        c.close();
    }

    // Deletes all exercises from the activity table
    public void deleteAllFoods() {
        Cursor c = getAllFoods();
        long rowID = c.getColumnIndexOrThrow(KEY_FOOD_ID);
        if (c.moveToFirst()) {
            do {
                deleteFoodRow(c.getLong((int) rowID));
            } while (c.moveToNext());
        }
        c.close();
    }

    // Deletes all targets from the target table
    public void deleteAllTargets() {
        Cursor c = getAllTargets();
        long rowID = c.getColumnIndexOrThrow(KEY_TARGET_ID);
        if (c.moveToFirst()) {
            do {
                deleteTargetRow(c.getLong((int) rowID));
            } while (c.moveToNext());
        }
        c.close();
    }

    // Gets all of the exercises in activities table
    public Cursor getAllActivities() {
        String where = null;
        Cursor c = db.query(TABLE_ACTIVITY, ALL_ACTIVITY_KEYS, where, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Gets all bmi rows in the database
    public Cursor getAllBmi() {
        String where = null;
        Cursor c = db.query(true, TABLE_BMI, ALL_BMI_KEYS, where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Gets all bmi rows in the database
    public Cursor getAllFoods() {
        String where = null;
        Cursor c = db.query(true, TABLE_FOOD, ALL_FOOD_KEYS, where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Gets all bmi rows in the database
    public Cursor getAllTargets() {
        String where = null;
        Cursor c = db.query(true, TABLE_TARGET, ALL_TARGET_KEYS, where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Gets a food calories for the day
    public float getFoodToday(String searchDate) {

        Cursor c = db.query(true, TABLE_FOOD, null, KEY_FOOD_DATE + " BETWEEN ? AND ?", new String[]{
                searchDate + "\n00:00:00", searchDate + "\n23:59:59"}, null, null, null, null);
        float totalCaloriesConsumed = 0;
        if (c.moveToFirst()) {
            do {
                totalCaloriesConsumed += c.getFloat(4);
            } while (c.moveToNext());
        }
        return totalCaloriesConsumed;
    }

    // Gets calories burnt for the day
    public float getActivityToday(String searchDate) {

        Cursor c = db.query(true, TABLE_ACTIVITY, null, KEY_ACTIVITY_DATE + " BETWEEN ? AND ?", new String[]{
                searchDate + "\n00:00:00", searchDate + "\n23:59:59"}, null, null, null, null);
        float totalCaloriesBurnt = 0;
        if (c.moveToFirst()) {
            do {
                totalCaloriesBurnt += c.getFloat(4);
            } while (c.moveToNext());
        }
        return totalCaloriesBurnt;
    }
    // Gets a specific activity entry
    public Cursor getActivityRow(long rowID) {
        String where = KEY_ACTIVITY_ID + "=" + rowID;
        Cursor c = db.query(true, TABLE_ACTIVITY, ALL_ACTIVITY_KEYS, where, null, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Gets the latest bmi entry
    public Cursor getLastBmiRow() {
        Cursor c = db.query(true, TABLE_BMI, ALL_BMI_KEYS, null, null, null, null, null, null);
        if (c != null) {
            c.moveToLast();
            return c;
        }
        return null;
    }

    // Gets a specific bmi entry
    public Cursor getBmiRow(long rowID) {
        String where = KEY_ROWID + "=" + rowID;
        Cursor c = db.query(true, TABLE_BMI, ALL_BMI_KEYS, where, null, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Gets a specific food entry
    public Cursor getFoodRow(long rowID) {
        String where = KEY_FOOD_ID + "=" + rowID;
        Cursor c = db.query(true, TABLE_FOOD, ALL_FOOD_KEYS, where, null, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    // Updates an activity record
    public boolean updateActivityRow(long rowID, String date, String duration, String exercise, String caloriesBurnt) {
        String where = KEY_ACTIVITY_ID + "=" + rowID;
        ContentValues cv = new ContentValues();
        cv.put(KEY_ACTIVITY_DATE, date);
        cv.put(KEY_ACTIVITY_DURATION, duration);
        cv.put(KEY_ACTIVITY_EXERCISE, exercise);
        cv.put(KEY_ACTIVITY_CALORIES_BURNT, caloriesBurnt);
        // Insert data into database
        return db.update(TABLE_ACTIVITY, cv, where, null) != 0;
    }

    // Updates a bmi record
    public boolean updateBmiRow(long rowID, String date, String age, String weight, String height, String bmiScore, String bmiClassification) {
        String where = KEY_ROWID + "=" + rowID;
        ContentValues cv = new ContentValues();
        cv.put(KEY_DATE, date);
        cv.put(KEY_AGE, age);
        cv.put(KEY_WEIGHT, weight);
        cv.put(KEY_HEIGHT, height);
        cv.put(KEY_BMISCORE, bmiScore);
        cv.put(KEY_BMICLASSIFICATION, bmiClassification);
        return db.update(TABLE_BMI, cv, where, null) != 0;
    }

    // Updates an food record
    public boolean updateFoodRow(long rowID, String date, String item, String portion, String calories) {
        String where = KEY_ACTIVITY_ID + "=" + rowID;
        ContentValues cv = new ContentValues();
        cv.put(KEY_FOOD_DATE, date);
        cv.put(KEY_FOOD_ITEM, item);
        cv.put(KEY_FOOD_PORTION, portion);
        cv.put(KEY_FOOD_CALORIES, calories);
        // Insert data into database
        return db.update(TABLE_ACTIVITY, cv, where, null) != 0;
    }

    // Updates a target record
    public boolean updateTargetRow(long rowID, String target, String timeframe, String calorieLossGain) {
        String where = KEY_TARGET_ID + "=" + rowID;
        ContentValues cv = new ContentValues();
        cv.put(KEY_TARGET_WEIGHT, target);
        cv.put(KEY_TARGET_TIME, timeframe);
        cv.put(KEY_TARGET_LOSS_RATE, calorieLossGain);
        // Insert data into database
        return db.update(TABLE_TARGET, cv, where, null) != 0;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase _db) {
            _db.execSQL(CREATE_TABLE_BMI);
            _db.execSQL(CREATE_TABLE_ACTIVITY);
            _db.execSQL(CREATE_TABLE_FOOD);
            _db.execSQL(CREATE_TABLE_TARGET);
        }

        @Override
        public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading applications database version from " + oldVersion + " to " + newVersion + ",which will format all existing data");
            // Remove old database
            _db.execSQL("DROP TABLE IF EXISTS " + TABLE_BMI);
            _db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITY);
            _db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOOD);
            _db.execSQL("DROP TABLE IF EXISTS " + TABLE_TARGET);
            // Re-create database
            onCreate(_db);
        }
    }
}
