package com.rol.HealthMonitor;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ExerciseActivity extends Fragment {
    static final int DATE_DIALOG = 0;
    static final int TIME_START_DIALOG = 1;
    Time today = new Time(Time.getCurrentTimezone());
    Calendar cDate = Calendar.getInstance();
    Calendar cStart = Calendar.getInstance();
    EditText et_date, et_time, et_hour, et_minute;
    TextView tv_today_exercise;
    Spinner exerciseSpinner;
    SimpleDateFormat format;
    Button btn_save, bt_delete_all_activites;
    ListView lv;
    private DBAdapter mDb;
    private TimePickerDialog.OnTimeSetListener mTimeStartSetListener =
            new TimePickerDialog.OnTimeSetListener() {

                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                    // TODO Auto-generated method stub
                    cStart.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    cStart.set(Calendar.MINUTE, minute);
                    et_time.setText(format.format(cStart.getTime()));
                }
            };
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {

                    // TODO Auto-generated method stub
                    cDate.set(Calendar.YEAR, year);
                    cDate.set(Calendar.MONTH, monthOfYear);
                    cDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    SimpleDateFormat format = new SimpleDateFormat("EEE dd-MM-yy");
                    et_date.setText(format.format(cDate.getTime()));
                }
            };

    private void openDB() {
        mDb = new DBAdapter(getActivity());
        mDb.open();
    }

    @Override
    public View onCreateView(LayoutInflater inflate, ViewGroup container, Bundle savedInstanceState) {

        View exerciseView = inflate.inflate(R.layout.fragment_exercise, container, false);
        // db = new SQLController(exerciseView.getContext());
        Initialize(exerciseView);
        // Populate recent activities list
        populateListView(exerciseView);
        return exerciseView;

    }

    private void Initialize(final View exerciseView) {
        // Open our  database
        openDB();
        // Setup button handlers
        initialiseButtonHandlers(exerciseView);
        // Initialise UI widgets
        et_date = (EditText) exerciseView.findViewById(R.id.et_date);
        et_date.setInputType(InputType.TYPE_NULL);

        et_hour = (EditText) exerciseView.findViewById(R.id.et_hour);
        et_minute = (EditText) exerciseView.findViewById(R.id.et_minutes);
        format = new SimpleDateFormat("dd-MM-yyyy");

        et_date.setText(format.format(cDate.getTime()));
        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateDialog(DATE_DIALOG).show();
            }
        });

        et_time = (EditText) exerciseView.findViewById(R.id.et_time);
        et_time.setInputType(InputType.TYPE_NULL);
        format = new SimpleDateFormat("hh:mm:ss");

        et_time.setText(format.format(cStart.getTime()));
        et_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateDialog(TIME_START_DIALOG).show();
            }
        });

        exerciseSpinner = (Spinner) exerciseView.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(exerciseView.getContext(), R.array.exercise_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        exerciseSpinner.setAdapter(adapter);

        tv_today_exercise = (TextView) exerciseView.findViewById(R.id.tv_today_exercise);

        lv = (ListView) exerciseView.findViewById(R.id.listViewActivites);
        // Set on list item long click listener
        listViewItemLongClick(exerciseView);
        calcExerciseToday();
    }

    private void initialiseButtonHandlers(View v) {
        final View exerciseView = v;
        btn_save = (Button) exerciseView.findViewById(R.id.btn_save_exercise);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FindEmpty(v)) {
                    // Display results to the user
                    new AlertDialog.Builder(v.getContext())
                            .setTitle("Activity Details")
                            .setMessage("Date: " + et_date.getText().toString() +
                                    "\t\tTime: " + et_time.getText().toString() +
                                    "\n\nActivity:\n" + exerciseSpinner.getSelectedItem().toString() +
                                    " for " + et_hour.getText().toString() + " hours and " +
                                    et_minute.getText().toString() + " minutes" +
                                    "\n\nCalories Burned \n" +
                                    new DecimalFormat("#.##").format(CalculateCalorieBurn())) // Note format is done here as we want to keep precision of actual values
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Create a new exercise info entry
                                    ExerciseInfo exerciseInfo = new ExerciseInfo(et_date.getText().toString(),
                                            et_time.getText().toString(),
                                            exerciseSpinner.getSelectedItem().toString(),
                                            CalculateDuration(Integer.parseInt(et_hour.getText().toString()), Integer.parseInt(et_minute.getText().toString())),
                                            CalculateCalorieBurn());
                                    // Add the entry to the database
                                    saveActivityData(exerciseInfo);
                                    populateListView(exerciseView);
                                    calcExerciseToday();
                                }

                            })
                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                }
            }
        });
        bt_delete_all_activites = (Button) exerciseView.findViewById(R.id.bt_delete_all_activites);
        bt_delete_all_activites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FindEmpty(v)) {
                    // Display results to the user
                    new AlertDialog.Builder(v.getContext())
                            .setTitle("Delete All Activites?")
                            .setMessage("Are you sure you want to delete all activites!?") // Note format is done here as we want to keep precision of actual values
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Add the entry to the database
                                    mDb.deleteAllActivities();
                                    populateListView(exerciseView);
                                    calcExerciseToday();
                                }

                            })
                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                }
            }
        });
    }
    private void saveActivityData(ExerciseInfo info) {
        mDb.insertActivityRow(
                String.format("%s\n%s", info.GetDate(), info.GetTime()),
                String.valueOf(new DecimalFormat("#.##").format(info.GetDuration())),
                info.GetActivityType(),
                String.valueOf(new DecimalFormat("#.##").format(info.GetCaloriesBurnt()))
        );
        calcExerciseToday();
    }

    private float CalculateCalorieBurn() {
        // To calculate the Calories burnt the following formula was used
        //Calories Burned (kcal) =  METs x (WEIGHT_IN_KILOGRAM) x (DURATION_IN_HOUR)
        // Sourced from the Journal of Sports Sciences http://www.braydenwm.com/cal_vs_hr_ref_paper.pdf

        // Metabolic Equivalent rate is a multiplier value
        // MET CHART http://download.lww.com/wolterskluwer_vitalstream_com/PermaLink/MSS/A/MSS_43_8_2011_06_13_AINSWORTH_202093_SDC1.pdf
        // Default Metabolic Equivalent of Task rate
        float MET = 0.0f, duration = 0.0f;
        //Log.d("MET", exerciseSpinner.getSelectedItem().toString());
        if (exerciseSpinner.getSelectedItem().toString().equalsIgnoreCase("Running") ||
                exerciseSpinner.getSelectedItem().toString().equalsIgnoreCase("Ice Skating") ||
                exerciseSpinner.getSelectedItem().toString().equalsIgnoreCase("Swimming")) {
            MET = 7.0f;
        }
        else if (exerciseSpinner.getSelectedItem().toString().equalsIgnoreCase("Walking") )
            MET = 3.5f;
        else if (exerciseSpinner.getSelectedItem().toString().equalsIgnoreCase("Weight Lifting") )
            MET = 6.0f;
        else if (exerciseSpinner.getSelectedItem().toString().equalsIgnoreCase("Cycling") )
            MET = 7.5f;
        else
            MET = 5.5f;
        if (!FindEmpty(getView())) {
            duration = CalculateDuration(Integer.parseInt(et_hour.getText().toString()), Integer.parseInt(et_minute.getText().toString()));
        }
        return MET * GetLatestWeight() * duration;
    }

    private float CalculateDuration(int hours, int minutes) {
        return hours + (minutes / 60.0f);
    }

    private float GetLatestWeight() {
        String weight = "";
        // Get the latest bmi entry
        Cursor c = mDb.getLastBmiRow();
        if (c.getCount() > 0) {
            // Weight is the third column
            weight = c.getString(3);
            // Re-format back into a float and return
            return Float.valueOf(weight.replaceAll("[^0-9?!\\.]", ""));
        } else
            Toast.makeText(getActivity().getBaseContext(), "Please enter your BMI information for more accurate results.", Toast.LENGTH_LONG).show();
        return 0;
    }

    private void populateListView(View v) {
        // Get all of the info from database
        Cursor c = mDb.getAllActivities();
        // Check for nulls as we can delete any given row
        if (c == null) {
            Log.w("Exercise", "Null cursor");
        } else {

            // Match up the database rows to the custom list view (bmi_item_layout)
            String[] rowNames = new String[]{DBAdapter.KEY_ACTIVITY_DATE, DBAdapter.KEY_ACTIVITY_EXERCISE, DBAdapter.KEY_ACTIVITY_DURATION, DBAdapter.KEY_ACTIVITY_CALORIES_BURNT};
            int[] toViewIDs = new int[]{R.id.tv_activity_date, R.id.tv_food_item, R.id.tv_food_portion, R.id.tv_food_caloires};
            // Setup the cursor adapter with the custom layout
            SimpleCursorAdapter mCursorAdapter;
            mCursorAdapter = new SimpleCursorAdapter(v.getContext(), R.layout.exercise_list_layout, c, rowNames, toViewIDs, 0);
            //Set adapter to list view
            lv.setAdapter(mCursorAdapter);
        }
    }

    private void listViewItemLongClick(View v) {

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final long itemID = id;
                final View v = view;
                // Display results to the user
                new AlertDialog.Builder(view.getContext())
                        .setTitle("Delete Activity?")
                        .setMessage("Are you sure you wish to delete this activity!?") // Note format is done here as we want to keep precision of actual values
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mDb.deleteActivityRow(itemID);
                                Toast.makeText(v.getContext(), "Activity deleted!", Toast.LENGTH_SHORT).show();
                                populateListView(v);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();

                return false;
            }
        });
    }

    private void calcExerciseToday() {
        today.setToNow();
        String timestamp = today.format("%d-%m-%Y");
        tv_today_exercise.setText(String.valueOf(mDb.getActivityToday(timestamp)));
    }
    protected Dialog onCreateDialog(int id)
    {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        switch(id)
        {
            case TIME_START_DIALOG:

                return new TimePickerDialog(getActivity(), mTimeStartSetListener, hour,minute, false);


            case DATE_DIALOG:

                return new DatePickerDialog(getActivity(),mDateSetListener,year,month,day);
        }
        return null;
    }
    private boolean FindEmpty(View v)
    {
        // Check each edit text for empty strings for HH and MM just set to zero
        if (et_hour.getText().toString().isEmpty() || Float.parseFloat(et_hour.getText().toString()) >= 24.0f)
        {
            et_hour.setText("0");
            Toast.makeText(v.getContext(), "Not a suitable hour! Defaulting to zero", Toast.LENGTH_SHORT).show();
            // return false because we have corrected the hours edit text
            return false;
        } else if (et_minute.getText().toString().isEmpty() || Float.parseFloat(et_minute.getText().toString()) >= 60.0f)
        {
            et_minute.setText("0");
            Toast.makeText(v.getContext(), "Not a suitable minute! Defaulting to zero", Toast.LENGTH_SHORT).show();
            // return false because we have corrected the minutes              edit text
            return false;
        }
        if(et_date.getText().toString().matches(""))
        {
            Toast.makeText(v.getContext(),"Please enter an date.", Toast.LENGTH_LONG).show();
            return true;
        }
        if(et_time.getText().toString().matches(""))
        {
            Toast.makeText(v.getContext(),"Please enter an time.", Toast.LENGTH_LONG).show();
            return true;
        }
        // Otherwise return that no empty fields were found
        return false;
    }
}
