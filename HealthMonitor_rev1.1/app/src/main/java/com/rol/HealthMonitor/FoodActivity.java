package com.rol.HealthMonitor;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class FoodActivity extends Fragment {
    Time today = new Time(Time.getCurrentTimezone());
    private EditText et_portion;
    private TextView tv_today;
    private Button bt_add_food, bt_delete_all_food;
    private FoodInfo foodInfo;
    private Spinner food_spinner;
    private DBAdapter mDb;
    private ListView foodList;
    // Gets a writable database
    private void openDB() {
        mDb = new DBAdapter(getActivity());
        mDb.open();
    }
    @Override
    public View onCreateView(LayoutInflater inflate, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the fragment view
        final View foodView = inflate.inflate(R.layout.fragment_food_diary, container, false);
        // Initialise default settings
        initialiseActivity(foodView);
        return foodView;
    }

    // Parses UI inputs into food info class and calculates calories consumed
    private void parseFoodInfo() {
        if (et_portion.getText().toString().isEmpty()) {
            Toast.makeText(getActivity().getBaseContext(), "Portion size not set, defaulting to 1", Toast.LENGTH_SHORT).show();
            foodInfo.portionSize = 1;
        } else
            foodInfo.portionSize = Integer.parseInt(et_portion.getText().toString());
        foodInfo.selectedFood = food_spinner.getSelectedItem().toString();
        foodInfo.caloriesConsumed = foodInfo.GetValues(foodInfo.selectedFood) * foodInfo.portionSize;
    }

    // Inserts new entry into the database
    private void saveFoodData(FoodInfo info) {
        today.setToNow();
        String timestamp = today.format("%d-%m-%Y\n%H:%M:%S");

        mDb.insertFoodRow(timestamp,
                String.format("%s", foodInfo.selectedFood),
                String.format("%s", foodInfo.portionSize),
                String.format("%s", foodInfo.caloriesConsumed));
        Log.v("Food Info", String.format("%s", foodInfo.caloriesConsumed));
    }

    private void populateFoodListView(View v) {
        // Get all of the info from database
        Cursor cursor = mDb.getAllFoods();
        if (cursor == null) {
            Log.w("Food Info", "Null cursor");
        } else {
            // Match up the database rows to the custom list view (food_list_layout)
            String[] rowNames = new String[]{DBAdapter.KEY_FOOD_DATE, DBAdapter.KEY_FOOD_ITEM, DBAdapter.KEY_FOOD_PORTION, DBAdapter.KEY_FOOD_CALORIES};
            int[] toViewIDs = new int[]{R.id.tv_food_date, R.id.tv_food_item, R.id.tv_food_portion, R.id.tv_food_caloires};
            // Setup the cursor adapter with the custom layout
            SimpleCursorAdapter mCursorAdapter;
            mCursorAdapter = new SimpleCursorAdapter(getActivity().getBaseContext(), R.layout.food_list_layout, cursor, rowNames, toViewIDs, 0);

            //Set adapter to list view
            foodList.setAdapter(mCursorAdapter);
        }
    }

    private void foodListViewItemLongClick(View v) {

        foodList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final long itemID = id;
                final View v = view;
                // Display results to the user
                new AlertDialog.Builder(view.getContext())
                        .setTitle("Delete Food Entry?")
                        .setMessage("Are you sure you wish to delete this food entry!?") // Note format is done here as we want to keep precision of actual values
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mDb.deleteFoodRow(itemID);
                                Toast.makeText(v.getContext(), "Food item deleted!", Toast.LENGTH_SHORT).show();
                                populateFoodListView(v);
                                calcFoodToday();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();

                return false;
            }
        });
    }

    // Initialises the Food Activity initial values
    private void initialiseActivity(View foodView) {
        foodInfo = new FoodInfo();
        // Open our  database
        openDB();
        // Initalise Button handlers
        initaliseButtonHandlers(foodView);
        // Setup list view
        foodList = (ListView) foodView.findViewById(R.id.listViewFoods);
        // Populate the list view
        populateFoodListView(foodView);
        // Setup UI Widgets
        et_portion = (EditText) foodView.findViewById(R.id.et_portion);
        tv_today = (TextView) foodView.findViewById(R.id.tv_food_today);
        calcFoodToday();
        // Initialise food spinner
        initialiseFoodSpinner(foodView);
        // Activate on long item listener
        foodListViewItemLongClick(foodView);
    }

    private void initialiseFoodSpinner(View foodView) {
        food_spinner = (Spinner) foodView.findViewById(R.id.foodSpinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(foodView.getContext(), R.layout.spinner_item);
        adapter.addAll(foodInfo.GetKeys());
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        food_spinner.setAdapter(adapter);
    }

    private void calcFoodToday() {
        today.setToNow();
        String timestamp = today.format("%d-%m-%Y");
        tv_today.setText(String.valueOf(mDb.getFoodToday(timestamp)));
    }

    private void initaliseButtonHandlers(View v) {
        final View foodView = v;
        // Initalise add food widget
        bt_add_food = (Button) foodView.findViewById(R.id.bt_add_food);
        // Show user info
        bt_add_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Parse UI Widgets into food info class & calculate calories
                parseFoodInfo();
                // Display results to the user
                new AlertDialog.Builder(foodView.getContext())
                        .setTitle("Your Food Entry")
                        .setMessage(foodInfo.selectedFood + "\nPortion:\n" + foodInfo.portionSize + "\nCalories:\n" + foodInfo.caloriesConsumed)
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Adds information into the database
                                saveFoodData(foodInfo);
                                // Re-populate the list view
                                populateFoodListView(foodView);
                                // Calculate number of calories today
                                calcFoodToday();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();
            }

        });
        // Initalise Delete all widget
        bt_delete_all_food = (Button) foodView.findViewById(R.id.bt_delete_all_foods);
        // Set delete all button handler
        bt_delete_all_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Parse UI Widgets into food info class & calculate calories
                parseFoodInfo();
                // Display results to the user
                new AlertDialog.Builder(foodView.getContext())
                        .setTitle("Delete All?")
                        .setMessage("Are you sure you want to delete all food entries?")
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Adds information into the database
                                mDb.deleteAllFoods();
                                // Re-populate the list view
                                populateFoodListView(foodView);
                                // Calculate number of calories today
                                calcFoodToday();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();
            }
        });
    }

}
